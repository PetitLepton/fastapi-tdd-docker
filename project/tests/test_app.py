from app import __version__


def test_version():
    assert __version__ == "0.1.0"


def test_ping(test_app):
    response = test_app.get("/ping")
    assert response.status_code == 200
    assert response.json() == {
        "environment": "dev",
        "ping": "pong!",
        "testing": True,
    }
